import React, {Component} from 'react';
import {Text, StyleSheet, View, Image} from 'react-native';
import {logo} from '../../img';

export default class index extends Component {
  componentDidMount() {
    setTimeout(() => {
      this.props.navigation.replace('Login');
    }, 3000);
  }
  render() {
    return (
      <View style={{flex: 1, alignItems: 'center', justifyContent: 'center'}}>
        <Image style={styles.logo} source={logo} />
        <Text style={{fontSize:20}}> Welcome! </Text>
      </View>
    );
  }
}

const styles = StyleSheet.create({
  logo: {
    marginBottom: 0,
    width: 100,
    height: 100,
    borderRadius: 20,
    marginTop: 70,
    alignSelf: 'center',
  },
});
