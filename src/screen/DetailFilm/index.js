import React, {Component} from 'react';
import {
  ActivityIndicator,
  FlatList,
  Text,
  View,
  StyleSheet,
  Image,
} from 'react-native';

export default class index extends Component {
  constructor(props) {
    super(props);

    this.state = {
      data: [],
      isLoading: true,
    };
  }

  async getMovies() {
    const {link} = this.props.route.params;
    try {
      const response = await fetch(
        `https://www.omdbapi.com/?i=${link}&apikey=997061b4`,
      );
      const json = await response.json();
      this.setState({data: [json]});
    } catch (error) {
      console.log(error);
    } finally {
      this.setState({isLoading: false});
    }
  }

  componentDidMount() {
    this.getMovies();
  }

  render() {
    const {link} = this.props.route.params;
    const {data, isLoading} = this.state;

    return (
      <View style={styles.content}>
        {isLoading ? (
          <ActivityIndicator />
        ) : (
          <FlatList
            data={data}
            // keyExtractor={({ id }, index) => id}
            renderItem={({item}) => (
              <View style={styles.container}>
                <Image
                  style={{width: 200, height: 200}}
                  source={{uri: `${item.Poster}`}}
                />
                <Text>
                  {item.Title} ({item.Year})
                </Text>
                <Text> Rated : {item.Rated} </Text>
                <Text> Released : {item.Released} </Text>
                <Text> Runtime : {item.Runtime} </Text>
                <Text> Genre : {item.Genre} </Text>
                <Text> Director : {item.Director} </Text>
                <Text> Writer : {item.Writer} </Text>
                <Text> Actors : {item.Actors} </Text>
                <Text> Plot : {item.Plot} </Text>
                <Text> Language : {item.Language} </Text>
                <Text> Country : {item.Country} </Text>
                <Text> Awards : {item.Awards} </Text>
              </View>
            )}
          />
        )}
      </View>
    );
  }
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    alignItems: 'center',
    justifyContent: 'center',
    padding: 20
  },
});
