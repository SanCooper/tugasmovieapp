import * as React from 'react';
import {NavigationContainer} from '@react-navigation/native';
import {createNativeStackNavigator} from '@react-navigation/native-stack';
import FilmLlist from '../screen/FilmList';
import DetailFilm from '../screen/DetailFilm';
import Login from '../screen/Login';
import Splash from '../screen/Splash';

const Stack = createNativeStackNavigator();
function index() {
  return (
    <NavigationContainer>
      <Stack.Navigator initialRouteName='Splash'>
        <Stack.Screen name="Login" component={Login} options={{headerShown: false}}/>
     
        <Stack.Screen name="Daftar Film" component={FilmLlist} />
        <Stack.Screen name="Detail Film" component={DetailFilm} />
        <Stack.Screen name="Splash" component={Splash} options={{headerShown: false}}/>

      </Stack.Navigator>
    </NavigationContainer>
  );
}
export default index;
