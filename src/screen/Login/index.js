import React, {Component} from 'react';
import {
  StyleSheet,
  Text,
  View,
  TextInput,
  TouchableOpacity,
  Image,
  Alert,
  Button,
  Linking,
  ScrollView,
  ToastAndroid,
} from 'react-native';
import axios from 'axios';
import {logo} from '../../img';
import Feather from 'react-native-vector-icons/Feather';
import FontAwesome from 'react-native-vector-icons/FontAwesome';

export default class App extends Component {
  constructor() {
    super();
    this.state = {
      data: [],
      username: '',
      password: '',
    };
  }

  async componentDidMount() {
    axios
      .get('https://dummyjson.com/users')
      .then(res => this.setState({data: res.data.users}))
      .catch(error => console.log(error));
  }

  _login() {
    const {data, username, password} = this.state;
    const {navigation} = this.props;
    const newData = data.filter(value => {
      return value.username == username && value.password == password;
    });
    if (newData.length > 0) {
      navigation.navigate('Daftar Film');
      console.log('Berhasil');
    } else {
      Alert.alert('Login Gagal!', 'Username atau Password salah.');
    }
    // newData.length > 0 ? console.log('berhasil') : console.log('gagal');
    console.log(newData);
  }

  render() {
    // this.state.data.length > 0 && console.log(this.state.data);
    const {data} = this.state;
    // console.log(data);
    return (
      <View style={styles.container}>
          <View>
          <Image style={styles.logo} source={logo} />
          </View>
          <View style={styles.container2}>
          <ScrollView style={{flex:1, width:'100%'}}>
            <View style={styles.card}>
              <View style={styles.loginContainer}>
                <Feather
                  name="user"
                  color="#000"
                  size={20}
                  style={{color: 'white'}}
                />
                <TextInput
                  style={styles.inputStyle}
                  placeholderTextColor="white"
                  placeholder="Username"
                  onChangeText={input => this.setState({username: input})}
                />
              </View>
              <View style={styles.loginContainer}>
                <Feather
                  name="lock"
                  color="#000"
                  size={20}
                  style={{color: 'white'}}
                />
                <TextInput
                  style={styles.inputStyle}
                  secureTextEntry
                  placeholderTextColor="white"
                  placeholder="Password"
                  onChangeText={input => this.setState({password: input})}
                />
              </View>
            </View>
            <View>
              <Button
                title="Login"
                onPress={() => {
                  this._login();
                }}
              />
            </View>
            <View style={{flex: 1, flexDirection: 'row', marginTop: 20, marginLeft:20}}>
              <Feather
                name="unlock"
                color="#000"
                size={18}
                style={{color: 'white'}}
              />
              <Text style={{color: 'white'}} onPress={() => {}}>
                {' '}
                FORGOT PASSWORD?
              </Text>
            </View>
            <View
              style={{
                flexDirection: 'row',
                alignItems: 'center',
                marginTop: 20,
              }}>
              <View
                style={{
                  flex: 1,
                  height: 2,
                  backgroundColor: 'white',
                  marginLeft: 20,
                }}
              />
              <View>
                <Text style={{width: 100, textAlign: 'center', color: 'white'}}>
                  Register With
                </Text>
              </View>
              <View
                style={{
                  flex: 1,
                  height: 2,
                  backgroundColor: 'white',
                  marginRight: 20,
                }}
              />
            </View>
            <View style={styles.registIcon}>
              <FontAwesome
                name="facebook-square"
                size={60}
                style={{marginRight: 20}}
                onPress={() => {
                  Linking.openURL('https://facebook.com');
                }}
              />
              <FontAwesome
                name="twitter-square"
                size={60}
                onPress={() => {
                  Linking.openURL('https://twitter.com');
                }}
              />
              <FontAwesome
                name="google-plus-square"
                size={60}
                style={{marginLeft: 20}}
                onPress={() => {
                  Linking.openURL('https://myaccount.google.com');
                }}
              />
            </View>
          </ScrollView>
          </View>
        </View>
    );
  }
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: 'white',
    justifyContent:'space-between'
  },
  container2: {
    flex: 1,
    backgroundColor: '#1aa7ec',
    alignItems: 'center',
    justifyContent: 'center',
    borderTopStartRadius: 30,
    borderTopEndRadius: 30,
    // borderRadius:20,
    height:"100%",
    width:'100%'
  },
  card: {
    flex: 1,
    width: '90%',
    padding: 10,
    borderRadius: 20,
    borderWidth: 1,
    margin: 20,
    borderColor: 'white',
  },
  logo: {
    marginBottom: 70,
    width: 100,
    height: 100,
    borderRadius: 20,
    marginTop: 70,
    alignSelf: 'center',
  },
  loginContainer: {
    flex: 1,
    flexDirection: 'row',
    justifyContent: 'center',
    alignItems: 'center',
    margin: 10,
  },
  inputStyle: {
    flex: 1,
    paddingTop: 0,
    paddingRight: 10,
    paddingBottom: 0,
    paddingLeft: 0,
    color: 'white',
    fontSize: 15,
    margin: 10,
    borderBottomColor: 'white',
    borderBottomWidth: 1,
  },
  registIcon: {
    flex: 1,
    margin: 20,
    flexDirection: 'row',
    alignItems:'center',
    justifyContent:'space-evenly'
  },
});
