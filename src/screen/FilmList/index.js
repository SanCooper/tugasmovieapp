import React, {Component} from 'react';
import {
  ActivityIndicator,
  FlatList,
  Text,
  View,
  StyleSheet,
  Image,
  Button
} from 'react-native';

export default class index extends Component {
  constructor(props) {
    super(props);

    this.state = {
      data: [],
      isLoading: true,
    };
  }

  async _getMovies() {
    try {
      const response = await fetch(
        'http://www.omdbapi.com/?s=avengers&apikey=997061b4',
      );
      const json = await response.json();
      this.setState({data: json.Search});
    } catch (error) {
      console.log(error);
    } finally {
      this.setState({isLoading: false});
    }
  }

  componentDidMount() {
    this._getMovies();
  }

  render() {
    const {data, isLoading} = this.state;
    const {navigation} = this.props

    return (
      <View>
        <View style={styles.content}>
          {isLoading ? (
            <ActivityIndicator />
          ) : (
            <FlatList
              data={data}
              //keyExtractor={({ id }, index) => id}
              renderItem={({item}) => (
                <View style={styles.separator}>
                  <View style={{paddingRight: 20}}>
                    <Image
                      style={{width: 75, height: 100}}
                      source={{uri: `${item.Poster}`}}
                    />
                  </View>
                  <View style={{flex: 1}}>
                    <Text style={{flex: 1}}>
                      {item.Title} ({item.Year})
                      {'\n'}
                      {item.Type}
                    </Text>
                    <Button title="Detail Film"
                        onPress={() => navigation.navigate('Detail Film', {link: item.imdbID})} />
                  </View>
                </View>
              )}
            />
          )}
        </View>
      </View>
    );
  }
}

const styles = StyleSheet.create({
  separator: {
    flex: 1,
    borderBottomWidth: 1,
    paddingBottom: 10,
    paddingTop: 10,
    flexDirection: 'row'
  },
  content: {
    paddingRight: 10,
    paddingLeft: 10,
  },
  header: {
    alignItems: 'center',
    paddingTop: 10,
    borderBottomWidth: 1,
  },
  headerText: {
    fontSize: 20,
    fontWeight: 'bold',
    paddingBottom: 15,
  },
});
